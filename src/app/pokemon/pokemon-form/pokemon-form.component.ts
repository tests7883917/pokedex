import { Component, Input } from '@angular/core';
import { Pokemon } from '../pokemon.models';
import { PokemonService } from '../pokemon.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: `./pokemon-form.component.html`,
  styleUrls: [
    `./pokemon-form.component.css`,
  ]
})
export class PokemonFormComponent { 

  @Input() pokemon: Pokemon | undefined;
  types: string[] = [];
  @Input() isCreateForm: boolean = false;
  buttonText: string = "";


  constructor(
    public pokemonService: PokemonService, 
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.types = this.pokemonService.getTypesList();
    if (this.isCreateForm) {
      this.buttonText = "Ajouter";
      if(this.pokemon){
        this.pokemon.picture = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRf3QVzvyKR-qzIE7HQA2svVEDaMqQ7rU3HA&s';
      }
    } else {
      this.buttonText = "Enregistrer les modifications"
    }
  }


  /** Cette fonction permet de vérifier si le pokemon possède le type passé en paramètre */
  hasType(type: string): boolean {
    return this.pokemon?.types.includes(type) || false;
  }

  /* Cette fonction permet d'ajouter le type passé en paramètre sur le pokémon en cours d'édition, si le typ était déjà affecté au pokémon, il sera retiré de celui ci */
  selectType($event: Event, type: string): void {
    const isChecked: boolean = ($event.target as HTMLInputElement).checked;

    if (isChecked){
      this.pokemon?.types.push(type)
    } else {
      const index = this.pokemon?.types.indexOf(type);
      if(index) {
        this.pokemon?.types.splice(index, 1);
      }
    }
  }

  onSubmit() {
    if(this.isCreateForm) {
      this.pokemonService.createPokemon(this.pokemon).subscribe((createdPokemon) => {
        this.pokemon = createdPokemon;
        this.router.navigate(['/pokemons']);        
      })
    } else {
      this.pokemonService.updatepokemon(this.pokemon).subscribe(() => {
          this.router.navigate(['/pokemons']);
        })
    }
  }
}