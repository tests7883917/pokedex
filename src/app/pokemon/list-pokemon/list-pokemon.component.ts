import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "../pokemon.models";
import { PokemonService } from "../pokemon.service";

@Component({
  selector: "app-list-pokemon",
  templateUrl: "./list-pokemon.component.html",
  styles: [],
})
export class ListPokemonComponent {
  pokemonList: Pokemon[] = [];
  pokemonSelected: Pokemon | undefined;


  constructor(private router: Router, public pokemonService: PokemonService) {}

  ngOnInit() {
    
    this.pokemonService.getPokemonList()
    .subscribe(pokemons => this.pokemonList = pokemons)
    console.log(this.pokemonList);
  }

  get sortedPokemonList(): Pokemon[] {
    return this.pokemonList
      .sort((a, b) => {
        return new Date(b.created).getTime() - new Date(a.created).getTime();
      });
  }


  viewPokemonDetail(pokemonId: number) {
    this.router.navigate(["/pokemon", pokemonId]);
  }

  addPokemon(): void {
    this.router.navigate(["/add/pokemon"]);
  }
}
