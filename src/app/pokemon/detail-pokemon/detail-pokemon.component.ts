import { Component } from '@angular/core';
import { Pokemon } from '../pokemon.models';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: [
    './detail-pokemon.component.css'
  ],
})
export class DetailPokemonComponent {
  pokemonList: Pokemon[] = [];
  pokemon: Pokemon|undefined;



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public pokemonService: PokemonService,
  ) {}

  ngOnInit(){
    this.pokemonService.getPokemonList()
      .subscribe(pokemons => {
        this.pokemonList = pokemons;        
  
        const pokemonId: string | null = this.route.snapshot.paramMap.get('id');
        
        if(pokemonId) {
          this.pokemon = this.pokemonList.find(pokemon => pokemon.id == +pokemonId );
        }
      })
  }

  goBack(): void {
    this.router.navigate(['pokemons']);
  }

  goToEditPage(id: number): void {
    this.router.navigate(['edit/pokemon', id]);
  }
}
