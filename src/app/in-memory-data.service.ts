import { Injectable } from '@angular/core';
import { POKEMONS } from './pokemon/api-pokemon';
import { InMemoryDbService, RequestInfo, ResponseOptions, STATUS } from 'angular-in-memory-web-api';


@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService extends InMemoryDbService{

  private pokemonCounter = 13;

  constructor() {
    super();
  }


  createDb() {
    let pokemons = POKEMONS;
    return { pokemons };
  }

  // Méthode pour obtenir un nouvel ID de Pokémon
  private getNewPokemonId(): number {
    return this.pokemonCounter++;
  }

  post(reqInfo: RequestInfo) {
    if (reqInfo.collectionName === 'pokemons') {
      const pokemon = reqInfo.utils.getJsonBody(reqInfo.req);
      const pokemonWithId = { ...pokemon, id: this.getNewPokemonId() };
      POKEMONS.push(pokemonWithId);
      return reqInfo.utils.createResponse$(() => {
        return {
          status: STATUS.OK,
          headers: reqInfo.headers,
          body: pokemonWithId
        };
      });
    }

    return undefined;
  }


}
