import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, delay, of, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn: boolean = false;

  constructor(private router: Router) { }

  login(name: string, password: string): Observable<boolean> {
    let isLoggedIn = false;
    if(name == 'pikachu' && password == 'carapuce') {
      isLoggedIn = true;
    }
    return of(isLoggedIn).pipe(
      delay(1000),
      tap(isLoggedIn => this.isLoggedIn = isLoggedIn)
    );
  }

  logout() {
    this.isLoggedIn = false
    this.router.navigate(['/login']);
  }
}
